package edu.washington.gs.maccoss.encyclopedia.utils.math;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.math3.distribution.HypergeometricDistribution;

import edu.washington.gs.maccoss.encyclopedia.utils.Logger;
import edu.washington.gs.maccoss.encyclopedia.utils.io.TableParser;
import edu.washington.gs.maccoss.encyclopedia.utils.io.TableParserConsumer;
import edu.washington.gs.maccoss.encyclopedia.utils.io.TableParserMuscle;
import edu.washington.gs.maccoss.encyclopedia.utils.io.TableParserProducer;
import gnu.trove.map.hash.TObjectIntHashMap;

public class FishersExactTest {
	public static void main2(String[] args) {
		File[] files=new File("/Users/searleb/Downloads/emma/").listFiles();
		for (File f : files) {

			ArrayList<String> groups=new ArrayList<>();
			float[] groupTotals=new float[10];
			TObjectIntHashMap<String> headerMap=new TObjectIntHashMap<>();

			TableParserMuscle muscle=new TableParserMuscle() {
				@Override
				public void processRow(Map<String, String> row) {
					if (groups.size()==0) {
						for (String header : row.keySet()) {
							if ("protid".equals(header)) continue;

							String group=header.substring(0, 2);
							int groupIndex=-1;
							for (int i=0; i<groups.size(); i++) {
								if (groups.get(i).equals(group)) {
									groupIndex=i;
									break;
								}
							}
							if (groupIndex==-1) {
								groupIndex=groups.size();
								groups.add(group);
							}
							headerMap.put(header, groupIndex);
						}
						
					}
					for (Entry<String, String> entry : row.entrySet()) {
						if ("protid".equals(entry.getKey())) continue;
						
						int index=headerMap.get(entry.getKey());
						groupTotals[index]+=Float.parseFloat(entry.getValue());
					}

				}
				
				@Override
				public void cleanup() {
				}
			};

			BlockingQueue<Map<String, String>> blockingQueue=new LinkedBlockingQueue<Map<String, String>>();
			TableParserProducer producer=new TableParserProducer(blockingQueue, f, "\t", 1);
			TableParserConsumer consumer=new TableParserConsumer(blockingQueue, muscle);

			Thread producerThread=new Thread(producer);
			Thread consumerThread=new Thread(consumer);
			producerThread.start();
			consumerThread.start();

			try {
				producerThread.join();
				consumerThread.join();
			} catch (InterruptedException ie) {
				Logger.errorLine("Percolator reading interrupted!");
				Logger.errorException(ie);
			}

			System.out.println(f.getName()+": Comparing between "+General.toString(groups.toArray())+"\tpvalue");

			muscle=new TableParserMuscle() {
				@Override
				public void processRow(Map<String, String> row) {
					String protein=null;
					float[] data=new float[groups.size()];
					
					for (Entry<String, String> entry : row.entrySet()) {
						if ("protid".equals(entry.getKey())) {;
							protein=entry.getValue();
							continue;
						}
						
						int index=headerMap.get(entry.getKey());
						
						data[index]+=Float.parseFloat(entry.getValue());
					}
					
					double pvalue=getFishersExactPvalue(groupTotals[0], groupTotals[1], data[0], data[1]);
					System.out.println(protein+"\t"+pvalue);
				}
				
				@Override
				public void cleanup() {
				}
			};

			blockingQueue=new LinkedBlockingQueue<Map<String, String>>();
			producer=new TableParserProducer(blockingQueue, f, "\t", 1);
			consumer=new TableParserConsumer(blockingQueue, muscle);

			producerThread=new Thread(producer);
			consumerThread=new Thread(consumer);
			producerThread.start();
			consumerThread.start();

			try {
				producerThread.join();
				consumerThread.join();
			} catch (InterruptedException ie) {
				Logger.errorLine("Percolator reading interrupted!");
				Logger.errorException(ie);
			}
			System.out.println();
		}
	}

	public static void mainTest(String[] args) {
		File f=new File("/Users/searleb/Documents/students/george_sun/local_fishers_exact.txt");
		TableParser.parseTSV(f, new TableParserMuscle() {
			
			@Override
			public void processRow(Map<String, String> row) {
				String accession=row.get("peptide_protein_accession_numbers");
				int x1=Integer.parseInt(row.get("x [category=75]"));
				int x2=Integer.parseInt(row.get("x [category=76]"));
				int n1=Integer.parseInt(row.get("n [category=75]"));
				int n2=Integer.parseInt(row.get("n [category=76]"));
				double pvalue=getFishersExactPvalue(n1, n2, x1, x2);
				System.out.println(accession+"\t"+x1+"\t"+x2+"\t"+n1+"\t"+n2+"\t"+pvalue);
			}
			
			@Override
			public void cleanup() {
			}
		});
	}

	public static void main(String[] args) {
		File f=new File("/Users/searleb/Documents/OSU/teaching/proteomics_class/homework/final_exam_notes/fishers_exact.csv");
		TableParser.parseCSV(f, new TableParserMuscle() {
			
			@Override
			public void processRow(Map<String, String> row) {
				String accession=row.get("Sample");
				int x1=Integer.parseInt(row.get("x1"));
				int x2=Integer.parseInt(row.get("x2"));
				int n1=Integer.parseInt(row.get("n1"));
				int n2=Integer.parseInt(row.get("n2"));
				double pvalue=Math.min(getFishersExactPvalue(n1, n2, x1, x2), 1);
				System.out.println(accession+"\t"+x1+"\t"+x2+"\t"+n1+"\t"+n2+"\t"+pvalue);
			}
			
			@Override
			public void cleanup() {
			}
		});
	}
	
	public static double getFishersExactPvalue(float total1, float total2, float value1, float value2) {
		HypergeometricDistribution hyperDist=new HypergeometricDistribution(Math.round(total1+total2), Math.round(total1), Math.round(value1+value2));
		double pValue1=hyperDist.cumulativeProbability(Math.round(value1));

		hyperDist=new HypergeometricDistribution(Math.round(total1+total2), Math.round(total2), Math.round(value1+value2));
		double pValue2=hyperDist.cumulativeProbability(Math.round(value2));
		return Math.min(pValue1, pValue2);
	}

}
