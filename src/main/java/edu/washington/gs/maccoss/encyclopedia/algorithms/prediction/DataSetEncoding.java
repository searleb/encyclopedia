package edu.washington.gs.maccoss.encyclopedia.algorithms.prediction;

import org.nd4j.linalg.dataset.DataSet;

public interface DataSetEncoding {
	public DataSet encodeDataset();
}
