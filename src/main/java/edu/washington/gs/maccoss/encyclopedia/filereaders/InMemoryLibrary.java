package edu.washington.gs.maccoss.encyclopedia.filereaders;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;

import edu.washington.gs.maccoss.encyclopedia.datastructures.AminoAcidConstants;
import edu.washington.gs.maccoss.encyclopedia.datastructures.LibraryEntry;
import edu.washington.gs.maccoss.encyclopedia.datastructures.PSMData;
import edu.washington.gs.maccoss.encyclopedia.datastructures.PeptidePrecursor;
import edu.washington.gs.maccoss.encyclopedia.datastructures.Range;
import edu.washington.gs.maccoss.encyclopedia.datastructures.SearchParameters;

public class InMemoryLibrary implements LibraryInterface {
	private final ArrayList<LibraryEntry> entries;
		
	public InMemoryLibrary(ArrayList<LibraryEntry> entries) {
		this.entries=entries;
	}
	
	@Override
	public int size() {
		return entries.size();
	}
	
	@Override
	public String getName() {
		return "Testing library of "+entries.size()+" entries";
	}
	public ArrayList<LibraryEntry> getAllEntries(boolean sqrt, AminoAcidConstants aaConstants) throws IOException, SQLException, DataFormatException {
		if (sqrt) {
			return getSqrtEntries(entries);
		} else {
			return new ArrayList<LibraryEntry>(entries);
		}
	}
	
	private static ArrayList<LibraryEntry> getSqrtEntries(ArrayList<LibraryEntry> entries) {
		ArrayList<LibraryEntry> updated=new ArrayList<LibraryEntry>();
		for (LibraryEntry entry : entries) {
			updated.add(entry.sqrt());
		}
		return updated;
	}
	
	public HashMap<String, String> getAccessions(Collection<String> peptideSeqs) throws IOException, SQLException, DataFormatException {
		HashSet<String> set=new HashSet<>(peptideSeqs);
		HashMap<String, String> accessions=new HashMap<>();
		for (LibraryEntry entry : entries) {
			if (set.contains(entry.getPeptideSeq())) {
				accessions.put(entry.getPeptideSeq(), PSMData.accessionsToString(entry.getAccessions()));
			}
		}
		return accessions;
	}
	
	public ArrayList<LibraryEntry> getUnlinkedEntries(Range precursorMz, boolean sqrt, AminoAcidConstants aaConstants) throws IOException, SQLException, DataFormatException {
		return getEntries(precursorMz, sqrt, aaConstants);
	}

	@Override
	public ArrayList<LibraryEntry> getEntries(String peptideModSeq, byte charge, boolean sqrt) throws IOException, SQLException, DataFormatException {
		ArrayList<LibraryEntry> returnables=new ArrayList<LibraryEntry>();
		for (LibraryEntry entry : entries) {
			if (peptideModSeq.equals(entry.getPeptideModSeq())&&charge==entry.getPrecursorCharge()) {
				if (sqrt) {
					returnables.add(entry.sqrt());	
				} else {
					returnables.add(entry);
				}
			}
		}
		return returnables;
	}
	
	@Override
	public ArrayList<LibraryEntry> getEntries(ArrayList<PeptidePrecursor> peptides, boolean sqrt) throws IOException, SQLException, DataFormatException {
		ArrayList<LibraryEntry> returnables=new ArrayList<LibraryEntry>();
		for (LibraryEntry entry : entries) {
			for (PeptidePrecursor precursor : peptides) {
				if (entry.compareTo(precursor)==0) {
					if (sqrt) {
						returnables.add(entry.sqrt());	
					} else {
						returnables.add(entry);
					}
				}
			}
		}
		return returnables;
	}

	@Override
	public ArrayList<LibraryEntry> getEntries(Range precursorMz, boolean sqrt, AminoAcidConstants aaConstants) throws IOException, SQLException, DataFormatException {
		ArrayList<LibraryEntry> returnables=new ArrayList<LibraryEntry>();
		for (LibraryEntry entry : entries) {
			if (precursorMz.contains((float)entry.getPrecursorMZ())) {
				if (sqrt) {
					returnables.add(entry.sqrt());	
				} else {
					returnables.add(entry);
				}
			}
		}
		return returnables;
	}
	
	public Range getMinMaxMZ() throws IOException, SQLException {
		double min=Double.MAX_VALUE;
		double max=0.0;
		for (LibraryEntry entry : entries) {
			if (entry.getPrecursorMZ()>max) max=entry.getPrecursorMZ();
			if (entry.getPrecursorMZ()<min) min=entry.getPrecursorMZ();
		}
		return new Range((float)min, (float)max);
	}
	
	@Override
	public Optional<Path> getSource(SearchParameters parameters) {
		return Optional.empty();
	}
	
	@Override
	public List<Path> getSourceFiles() throws IOException, SQLException {
		return new ArrayList<>();
	}
}
