package edu.washington.gs.maccoss.encyclopedia.algorithms.prediction;

import org.nd4j.linalg.dataset.MultiDataSet;

public interface MultiDataSetEncoding {
	public MultiDataSet encodeDataset();
}
