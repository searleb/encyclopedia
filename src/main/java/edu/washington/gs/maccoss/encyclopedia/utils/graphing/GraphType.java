package edu.washington.gs.maccoss.encyclopedia.utils.graphing;

public enum GraphType {
	area, line, dashedline, bolddashedline, boldline, squaredline, bighollowpoint, bigpoint, point, spectrum, tinypoint, text, uncenteredText;
}
