package edu.washington.gs.maccoss.encyclopedia.utils.io;

public interface LineParserMuscle {
	public void processRow(String row);
	public void cleanup();
}
