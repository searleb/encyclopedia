package edu.washington.gs.maccoss.encyclopedia.algorithms;

import edu.washington.gs.maccoss.encyclopedia.datastructures.LibraryEntry;

public class RecalibratedPeptideScoringResult extends PeptideScoringResult {
	public RecalibratedPeptideScoringResult(LibraryEntry entry) {
		super(entry);
	}
}
