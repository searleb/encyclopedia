package edu.washington.gs.maccoss.encyclopedia.utils.math;

public interface RTProbabilityModel {

	float getProbability(float retentionTime, float delta);

}