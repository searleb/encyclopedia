package edu.washington.gs.maccoss.encyclopedia.datastructures;

public interface HasMZ {

	double getMZ();

}