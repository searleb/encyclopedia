package edu.washington.gs.maccoss.encyclopedia.utils.massspec;

public interface SpectrumWithCharge extends Spectrum {
	public byte getPrecursorCharge();
}
