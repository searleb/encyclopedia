package edu.washington.gs.maccoss.encyclopedia.datastructures;

import java.util.ArrayList;
import java.util.Comparator;

import edu.washington.gs.maccoss.encyclopedia.utils.math.General;

//@Immutable
public class Range implements Comparable<Range> {
	private final float start, stop;

	public Range(double start, double stop) {
		this((float)start, (float)stop);
	}
	public Range(float start, float stop) {
		// ensure that start comes before stop
		if (start<=stop) {
			this.start = start;
			this.stop = stop;
		} else {
			this.start = stop;
			this.stop = start;
		}
	}
	public Range(float[] data) {
		this(General.min(data), General.max(data));
	}
	
	public Range addBuffer(float buffer) {
		return new Range(start-buffer, stop+buffer);
	}
	
	@Override
	public String toString() {
		return Math.round(start*10.0f)/10.0f+" to "+Math.round(stop*10.0f)/10.0f;
	}
	
	@Override
	public int hashCode() {
		return Float.floatToIntBits(start)+16807*Float.floatToIntBits(stop);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Range)) return false;
		return compareTo((Range)obj)==0;
	}
	
	public float getStart() {
		return start;
	}
	
	public float getStop() {
		return stop;
	}
	
	public float getMiddle() {
		return (start+stop)/2.0f;
	}
	
	public float getRange() {
		return stop-start;
	}
	
	public boolean contains(double value) {
		if (value>=start&&value<=stop) {
			return true;
		}
		return false;
	}
	
	public boolean contains(float value) {
		if (value>=start&&value<=stop) {
			return true;
		}
		return false;
	}
	
	public boolean contains(Range value) {
		if (value.getStop()>=start&&value.getStart()<=stop) {
			return true;
		}
		return false;
	}
	
	public ArrayList<Range> chunkIntoBins(int binCount) {
		float delta=getRange()/binCount;
		
		ArrayList<Range> ranges=new ArrayList<Range>();
		float currentMin=start;
		for (int i=0; i<binCount; i++) {
			float currentMax=currentMin+delta;
			ranges.add(new Range(currentMin, currentMax));
			currentMin=currentMax;
		}
		return ranges;
	}
	
	/**
	 * sorts on start location, then on stop location
	 */
	public int compareTo(Range o) {
		if (o==null) return 1;
		if (start>o.start) return 1;
		if (start<o.start) return -1;
		if (stop>o.stop) return 1;
		if (stop<o.stop) return -1;
		return 0;
	}
	
	/**
	 * sorts on normal compareTo, but equals is inclusive of any range boundary intersection 
	 */
	public static Comparator<Range> RANGE_CONTAINS_COMPARATOR=new Comparator<Range>() {
		@Override
		public int compare(Range o1, Range o2) {
			if (o1==null&&o2==null) return 0;
			if (o1==null) return 1;
			if (o2==null) return -1;
			
			Range smaller, larger;
			if (o1.getRange()>o2.getRange()) {
				larger=o1;
				smaller=o2;
			} else {
				smaller=o1;
				larger=o2;
			}
			if (larger.contains(smaller.getStart())||larger.contains(smaller.getStop())) {
				return 0;
			}
			return o1.compareTo(o2);
		}
	};
	
	public float linearInterp(float X, float minY, float maxY) {
		float deltaX=getRange();
		if (deltaX==0) {
			float half=(maxY+minY)/2f;
			if (half<minY) return minY;
			if (half>maxY) return maxY;
			return half;
		}
		float deltaY=maxY-minY;
		if (deltaY==0) {
			return maxY;
		}
		float interp=((deltaY/deltaX)*(X-getStart())+minY);
		if (interp<minY) return minY;
		if (interp>maxY) return maxY;
		return interp;
	}
	
	public int linearInterp(float X, int minY, int maxY) {
		return Math.round(linearInterp(X, (float)minY, (float)maxY));
	}
	
	public float mapBackToRange(float Y, float minY, float maxY) {
		float deltaX=getRange();
		if (deltaX==0) {
			return getStop();
		}
		
		float deltaY=maxY-minY;
		if (deltaY==0) {
			float half=(getStart()+getStop())/2f;
			if (half<getStart()) return getStart();
			if (half>getStop()) return getStop();
			return half;
		}
		float interp=((deltaX/deltaY)*(Y-minY)+getStart());
		if (interp<getStart()) return getStart();
		if (interp>getStop()) return getStop();
		return interp;
	}
	
	public static Range getWidestRange(ArrayList<Range> ranges) {
		float min=Float.MAX_VALUE;
		float max=-Float.MAX_VALUE;
		for (Range range : ranges) {
			if (range.getStart()<min) {
				min=range.getStart();
			}
			if (range.getStop()>max) {
				max=range.getStop();
			}
		}
		return new Range(min, max);
	}
}
