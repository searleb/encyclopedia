package edu.washington.gs.maccoss.encyclopedia.algorithms;

import edu.washington.gs.maccoss.encyclopedia.datastructures.FragmentScan;

public class RecalibratedSpectrumScoringResult extends SpectrumScoringResult {
	public RecalibratedSpectrumScoringResult(FragmentScan entry) {
		super(entry);
	}
}
