package edu.washington.gs.maccoss.encyclopedia.algorithms;

import edu.washington.gs.maccoss.encyclopedia.datastructures.FragmentScan;

public class RescoredSpectrumScoringResult extends SpectrumScoringResult {
	public RescoredSpectrumScoringResult(FragmentScan entry) {
		super(entry);
	}
}
