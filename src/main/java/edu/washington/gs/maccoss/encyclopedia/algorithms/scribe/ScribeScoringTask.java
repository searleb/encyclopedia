package edu.washington.gs.maccoss.encyclopedia.algorithms.scribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;

import edu.washington.gs.maccoss.encyclopedia.Scribe;
import edu.washington.gs.maccoss.encyclopedia.algorithms.AbstractLibraryScoringTask;
import edu.washington.gs.maccoss.encyclopedia.algorithms.AbstractScoringResult;
import edu.washington.gs.maccoss.encyclopedia.algorithms.EValueCalculator;
import edu.washington.gs.maccoss.encyclopedia.algorithms.IsotopicDistributionCalculator;
import edu.washington.gs.maccoss.encyclopedia.algorithms.PSMScorer;
import edu.washington.gs.maccoss.encyclopedia.algorithms.SpectrumScoringResult;
import edu.washington.gs.maccoss.encyclopedia.algorithms.xcordia.allelespecific.SimilarPeptideBinner;
import edu.washington.gs.maccoss.encyclopedia.datastructures.FragmentScan;
import edu.washington.gs.maccoss.encyclopedia.datastructures.FragmentationModel;
import edu.washington.gs.maccoss.encyclopedia.datastructures.LibraryEntry;
import edu.washington.gs.maccoss.encyclopedia.datastructures.PrecursorScanMap;
import edu.washington.gs.maccoss.encyclopedia.datastructures.Range;
import edu.washington.gs.maccoss.encyclopedia.datastructures.SearchParameters;
import edu.washington.gs.maccoss.encyclopedia.gui.general.Charter;
import edu.washington.gs.maccoss.encyclopedia.utils.Nothing;
import edu.washington.gs.maccoss.encyclopedia.utils.graphing.XYTraceInterface;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.MassConstants;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.MassTolerance;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.PeptideUtils;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.SparseXCorrCalculator;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.SparseXCorrSpectrum;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.Spectrum;
import edu.washington.gs.maccoss.encyclopedia.utils.math.General;
import edu.washington.gs.maccoss.encyclopedia.utils.math.Log;
import edu.washington.gs.maccoss.encyclopedia.utils.math.ScoredIndex;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.map.hash.TFloatFloatHashMap;

public class ScribeScoringTask extends AbstractLibraryScoringTask {
	// FIXME shouldn't be static (long term)
	private static final float SCORE_CONSTANT=10f; // force positive scores for delta calculation
	private static final HashMap<String, SparseXCorrSpectrum> libraryEntryMap=new HashMap<String, SparseXCorrSpectrum>();
	private static final HashMap<String, float[]> isotopeDistributions=new HashMap<String, float[]>();
	private final ScribeScorer scorerFunction;
	private final Range fragmentRange=new Range(200, Float.MAX_VALUE); // remove peaks below 200 
	private static final int minimumNumberOfPeaks = 10;
	
	private static final int peaksKept=5;
	
	public ScribeScoringTask(PSMScorer scorer, ArrayList<LibraryEntry> entries, ArrayList<FragmentScan> stripes, PrecursorScanMap precursors, BlockingQueue<AbstractScoringResult> resultsQueue, SearchParameters parameters) {
		super(scorer, filterEntriesByScore(entries), stripes, precursors, resultsQueue, parameters);
		scorerFunction=(ScribeScorer)scorer;
	}
	
	public static ArrayList<LibraryEntry> filterEntriesByScore(ArrayList<LibraryEntry> entries) {
		HashMap<String, LibraryEntry> map=new HashMap<>();
		for (LibraryEntry entry : entries) {
			LibraryEntry prev=map.get(entry.getPeptideModSeq());
			if (prev==null||prev.getTIC()<entry.getTIC()) {
				// TODO Smart to prefer higher TIC? at least this keeps a consistent sort order
				map.put(entry.getPeptideModSeq(), entry);
			}
		}
		return new ArrayList<LibraryEntry>(map.values());
	}

	@Override
	protected Nothing process() {
		for (FragmentScan msms : super.stripes) {
			msms=msms.trimMasses(fragmentRange);
			XCorrCalculatorSpectrum xcorrMSMS=new XCorrCalculatorSpectrum(msms, parameters);
			if (xcorrMSMS.getNumPeaks()<minimumNumberOfPeaks) {
				//System.out.println("cut\t"+1);
				continue;
			}
			
			ArrayList<ScoredIndex> goodHits=new ArrayList<ScoredIndex>();
			TFloatFloatHashMap map=new TFloatFloatHashMap(); // x=index, y=score
			float maxScore=-SCORE_CONSTANT;
			String maxSequence=null;
			float secondMaxScore=-SCORE_CONSTANT;
			//float[] xcorrs=new float[super.entries.size()];
			for (int i=0; i<super.entries.size(); i++) {
				LibraryEntry entry=super.entries.get(i);
				boolean match=parameters.getPrecursorTolerance().equals(entry.getPrecursorMZ(), msms.getPrecursorMZ());
				for (int j = 0; j < Scribe.NUMBER_OF_ISOTOPES_ABOVE_MONOISOTOPIC; j++) {
					double target = entry.getPrecursorMZ()+(j+1)*MassConstants.neutronMass/entry.getPrecursorCharge();
					match=match||parameters.getPrecursorTolerance().equals(target, msms.getPrecursorMZ());	
				}
				// required charge
//				if (msms.getCharge()!=0&&msms.getCharge()!=entry.getPrecursorCharge()) {
//					match=false;
//				}
				if (match) {
					SparseXCorrSpectrum xcorrEntry=getXCorrEntry(entry);
					
					//float score=xcorrMSMS.score(xcorrEntry);
					//xcorrs[i]=score;
					float[] scores=score(entry, msms, parameters);
					float score=scores[1]+0.03f*scores[0];
					if (Float.isNaN(score)) continue;
					
					if (scores[0]>0) {
						//float composite=xcorrs[i]; // "main" score is based on xcorr
						//float composite=otherScores[0]; // "main" score is based on xtandem
						//float composite=otherScores[1]; // "main" score is based on sum of squared errors
						//float composite=otherScores[2]; // "main" score is based on dot product
						//float composite=otherScores[3]; // "main" score is based on spectral angle
						//float composite=otherScores[4]; // "main" score is based on logit
						
						//float composite=otherScores[1];//xcorrs[i]*0.37f+otherScores[0]*0.017f+otherScores[1]*0.61f+0.61f;
						goodHits.add(new ScoredIndex(score, i));
						map.put(i, score);
					
						if (maxSequence==null) {
							maxSequence=entry.getPeptideSeq();
							maxScore=score;
						} else if (score>maxScore) {
							if (!SimilarPeptideBinner.areSimilarEnough(maxSequence, entry.getPeptideSeq())) {
								secondMaxScore=maxScore;
							}
							maxSequence=entry.getPeptideSeq();
							maxScore=score;
						} else if (score>secondMaxScore) {
							if (!SimilarPeptideBinner.areSimilarEnough(maxSequence, entry.getPeptideSeq())) {
								secondMaxScore=score;
							}
						}
					}
				}
			}
			if (map.size()==0) { // maxXCorr==0 indicates no peaks
				//System.out.println("cut\t"+2);
				continue;
			}
			
			EValueCalculator calculator=new EValueCalculator(map, 0.1f);
			
			SpectrumScoringResult result=new SpectrumScoringResult(msms);
			Collections.sort(goodHits);
			//System.out.println("size\t"+map.size()+"\t"+calculator.getN()+"\t"+calculator.getM()+"\t"+calculator.getB()+"\t"+calculator.getNegLnEValue()+"\t"+super.entries.get(goodHits.get(goodHits.size()-1).y).getPeptideModSeq()+"\t"+msms.getScanStartTime());
			
			int identifiedPeaks=0;
			for (int i=goodHits.size()-1; i>=0; i--) {
				float score=goodHits.get(i).x;
				int index=goodHits.get(i).y;
				float evalue=calculator.getNegLnEValue(score);
				//float xcorr=xcorrs[index];
				
				float deltaScore=(maxScore+SCORE_CONSTANT<=0.0f||secondMaxScore+SCORE_CONSTANT<=0.0f)?-1.0f:((score-secondMaxScore)/(maxScore+SCORE_CONSTANT));

				LibraryEntry entry=super.entries.get(index);
				int chargeMatch=(msms.getPrecursorCharge()==0||msms.getPrecursorCharge()==entry.getPrecursorCharge())?1:0;
					
				float[] predictedIsotopeDistribution=getIsotopeDistribution(entry);
				float[] auxScoreArray=scorerFunction.auxScore(entry, msms, predictedIsotopeDistribution, precursors);
				if (auxScoreArray[0]<=0) {
					// dot product is 0, means no matching b/y peaks in the top scoring model
					//System.out.println("cut\t"+3);
					//continue;
				}
				float deltaPrecursorMass=auxScoreArray[scorerFunction.getParentDeltaMassIndex()];
				float deltaFragmentMass=auxScoreArray[scorerFunction.getFragmentDeltaMassIndex()];
				result.addPeptide(score, General.concatenate(auxScoreArray, evalue, deltaScore, map.size(), chargeMatch), deltaPrecursorMass, deltaFragmentMass, entry);
				
				if (identifiedPeaks>peaksKept) {
					// keep N+1 peaks
					break;
				}
				identifiedPeaks++;
			}
			resultsQueue.add(result);
		}
		return Nothing.NOTHING;
	}
	public SparseXCorrSpectrum getXCorrEntry(LibraryEntry entry) {
		SparseXCorrSpectrum xcorr=libraryEntryMap.get(entry.getPeptideModSeq());
		if (xcorr==null) {
			xcorr=SparseXCorrCalculator.normalize(entry, new Range((float)entry.getPrecursorMZ()-10f, (float)entry.getPrecursorMZ()+10f), false, parameters);
			libraryEntryMap.put(entry.getPeptideModSeq(), xcorr);
		}
		return xcorr;
		
	}

	public float[] getIsotopeDistribution(LibraryEntry entry) {
		float[] predictedIsotopeDistribution=isotopeDistributions.get(entry.getPeptideModSeq());
		if (predictedIsotopeDistribution==null) {
			predictedIsotopeDistribution=IsotopicDistributionCalculator.getIsotopeDistribution(entry.getPeptideModSeq(), parameters.getAAConstants());
			isotopeDistributions.put(entry.getPeptideModSeq(), predictedIsotopeDistribution);
		}
		return predictedIsotopeDistribution;
	}


	public static float[] score(LibraryEntry entry, Spectrum spectrum, SearchParameters parameters) {
		MassTolerance acquiredTolerance=parameters.getFragmentTolerance();
		MassTolerance libraryTolerance=parameters.getLibraryFragmentTolerance();
		FragmentationModel model=PeptideUtils.getPeptideModel(entry.getPeptideModSeq(), parameters.getAAConstants());
		double[] ions=model.getPrimaryIons(parameters.getFragType(), entry.getPrecursorCharge(), false);
		
		double[] predictedMasses=entry.getMassArray();
		float[] predictedIntensities=entry.getIntensityArray();
		float[] correlation=entry.getCorrelationArray();
		
		double[] acquiredMasses=spectrum.getMassArray();
		float[] acquiredIntensities=spectrum.getIntensityArray();

		int numberOfMatchingPeaks=0;
		TDoubleArrayList predictedTargets=new TDoubleArrayList();
		TFloatArrayList predictedTargetIntensities=new TFloatArrayList();
		TFloatArrayList actualTargetIntensities=new TFloatArrayList();
		for (double target : ions) {
			int[] predictedIndicies=libraryTolerance.getIndicies(predictedMasses, target);
			float predictedIntensity=0.0f;
			float maxCorrelation=0.01f;
			for (int i=0; i<predictedIndicies.length; i++) {
				if (predictedIntensity<predictedIntensities[predictedIndicies[i]]) {
					predictedIntensity=predictedIntensities[predictedIndicies[i]];
				}
				if (maxCorrelation<correlation[predictedIndicies[i]]) {
					maxCorrelation=correlation[predictedIndicies[i]];
				}
			}
			
			if (predictedIntensity>0) {
				int[] indicies=acquiredTolerance.getIndicies(acquiredMasses, target);
				float intensity=0.0f;
				float bestPeakIntensity=0.0f;
				for (int j=0; j<indicies.length; j++) {
					intensity+=acquiredIntensities[indicies[j]];
					
					if (acquiredIntensities[indicies[j]]>bestPeakIntensity) {
						bestPeakIntensity=acquiredIntensities[indicies[j]];
					}
				}
				if (intensity>0) {
					numberOfMatchingPeaks++;
				}
				predictedTargets.add(target);
				predictedTargetIntensities.add(predictedIntensity);
				actualTargetIntensities.add(intensity);
			}
		}

		float[] predictedTargetIntensitiesArray=General.normalizeToL2(predictedTargetIntensities.toArray());
		float[] actualTargetIntensitiesArray=General.normalizeToL2(actualTargetIntensities.toArray());
		
		float dotProduct=General.sum(General.multiply(predictedTargetIntensitiesArray, actualTargetIntensitiesArray));

		if (Float.isNaN(dotProduct)||dotProduct<0.0f) dotProduct=0.0f;
		float protectedDP=dotProduct;
		if (protectedDP>=1.0f) protectedDP=0.99999f;
		if (protectedDP<=0.0f) protectedDP=0.00001f;
		
		float contrastAngle=1.0f-(2.0f*(float)Math.acos(protectedDP))/(float)Math.PI;
		float logit=(float)Math.log(protectedDP/(1.0f-protectedDP));
		float sumOfSquaredErrors=0.0f; // normalized to sum of targeted intensities
		
		for (int i=0; i<predictedTargetIntensitiesArray.length; i++) {
			if (predictedTargetIntensitiesArray[i]>0.0||actualTargetIntensitiesArray[i]>0.0) {
				float delta=predictedTargetIntensitiesArray[i]-actualTargetIntensitiesArray[i];
				float deltaSquared=delta*delta;
				sumOfSquaredErrors+=deltaSquared;
			}
		}
		
		float xTandem;
		if (numberOfMatchingPeaks==0||dotProduct<=0) {
			xTandem=0.0f;
		} else {
			xTandem=((float)Log.protectedLog10(dotProduct))+Log.logFactorial(numberOfMatchingPeaks); // really log10(X!Tandem score)
		}
		
		float scribe;
		if (sumOfSquaredErrors==0.0f) {
			scribe=0.0f;
		} else {
			scribe=Log.protectedLn(1.0f/sumOfSquaredErrors);
		}
		
		return new float[] {xTandem, scribe, dotProduct, contrastAngle, logit};
	}
}
